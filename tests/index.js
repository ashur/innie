const {assert} = require( "chai" );
const fs = require( "fs" );
const innie = require( "../index" );
const path = require( "path" );
const {readFile} = require( "../utils" );

let tmpDir = path.join( __dirname, "fixtures/.tmp" );

let rmTmpDir = () =>
{
	let stats;
	try
	{
		stats = fs.statSync( tmpDir );
	}
	catch( error )
	{
		// If the directory doesn't exist, don't sweat it
	}

	if( stats && stats.isDirectory() )
	{
		let children = fs.readdirSync( tmpDir );
		children.forEach( child =>
		{
			fs.unlinkSync( path.join( tmpDir, child ) );
		});

		fs.rmdirSync( tmpDir );
	}
};

/**
 * @param {string} contents
 * @returns {string}
 */
function createFixture( slug, contents )
{
	let filename = path.join(
		tmpDir,
		`${Date.now()}-${slug}.ini`
	);

	fs.writeFileSync( filename, contents );

	return filename;
}

describe( "read", () =>
{
	before( () => fs.mkdirSync( tmpDir, { recursive: true } ) );
	after( rmTmpDir );

	it( "should throw error if file does not exist", () =>
	{
		let fixture = "./fixtures/non-existent.ini";

		let fn = () => innie.read( fixture, "foo" );
		assert.throws( fn, `No such file '${fixture}'` );
	});

	it( "should throw error if key is undefined", () =>
	{
		let key = "zoo";
		let fixture = createFixture( "read-undefined", "foo=bar" );

		let fn = () => innie.read( fixture, key );
		assert.throws( fn, `No such key '${key}'` );
	});

	it( "should return value if key is defined", () =>
	{
		let fixture = createFixture( "read-return", "foo=bar\n" );

		let actual = innie.read( fixture, "foo" );
		assert.equal( actual, "bar" );
	});

	it( "should support dot notation for nested keys", () =>
	{
		let fixture = createFixture("write-dot", "[boo]\nbaz=bar\n\n[foo]\nfaz=far\nfoz=for\n" );

		let actual = innie.read( fixture, "boo.baz" )
		assert.equal( actual, "bar" );
	});

	it( "should return INI encoded value if section requested", () =>
	{
		let fixture = createFixture("write-dot", "[boo]\nbaz=bar\n\n[foo]\nfaz=far\nfoz=for\n" );

		let actual = innie.read( fixture, "foo" )
		assert.equal( actual, "faz=far\nfoz=for" );
	});
});

describe( "write", () =>
{
	before( () => fs.mkdirSync( tmpDir, { recursive: true } ) );
	after( rmTmpDir );

	it( "should throw error if file does not exist", () =>
	{
		let fixture = "./fixtures/non-existent.ini";

		let fn = () => innie.write( fixture, "foo", "baz" );
		assert.throws( fn, `No such file '${fixture}'` );
	});

	it( "should append value if key is undefined", () =>
	{
		let fixture = createFixture( "write-append", "foo=bar" );
		innie.write( fixture, "zoo", "zar" );

		let actual = readFile( fixture );
		assert.equal( actual, "foo=bar\nzoo=zar\n" );
	});

	it( "should overwrite value if key is defined", () =>
	{
		let fixture = createFixture( "write-overwrite", "foo=bar\n" );
		innie.write( fixture, "foo", "zar" );

		let actual = readFile( fixture );
		assert.equal( actual, "foo=zar\n" );
	});

	it( "should support dot notation for nested keys", () =>
	{
		let fixture = createFixture( "write-dot", "[foo]\nbaz=bar" );

		innie.write( fixture, "boo.faz", "far" );
		let append = readFile( fixture );
		assert.equal( append, "[foo]\nbaz=bar\n\n[boo]\nfaz=far\n" );

		innie.write( fixture, "foo.baz", "zar" );
		let overwrite = readFile( fixture );
		assert.equal( overwrite, "[foo]\nbaz=zar\n\n[boo]\nfaz=far\n" );
	});
});

describe( "delete", () =>
{
	before( () => fs.mkdirSync( tmpDir, { recursive: true } ) );
	after( rmTmpDir );

	it( "should throw error if file does not exist", () =>
	{
		let fixture = "./fixtures/non-existent.ini";

		let fn = () => innie.delete( fixture, "foo" );
		assert.throws( fn, `No such file '${fixture}'` );
	});

	it( "should not change file if key does not exist", () =>
	{
		let fixture = createFixture( "delete-no-change", "[foo]\nbaz=bar" );
		innie.delete( fixture, "zoo" );

		let actual = readFile( fixture );
		assert.equal( actual, "[foo]\nbaz=bar" );
	});

	it( "should delete key if defined", () =>
	{
		let fixture = createFixture( "delete-key", "foo=bar\nzoo=zar" );
		innie.delete( fixture, "foo" );

		let actual = readFile( fixture );
		assert.equal( actual, "zoo=zar\n" );
	});

	it( "should support dot notation for nested keys", () =>
	{
		let contents = "[foo]\nfaz=far\nfiz=foz\n\n[boo]\nbaz=bar\n";

		let fixture = createFixture( "delete-dot", contents );
		innie.delete( fixture, "foo.faz" );

		let actual = readFile( fixture );
		assert.equal( actual, "[foo]\nfiz=foz\n\n[boo]\nbaz=bar\n" );
	});

	it( "should delete section if specified", () =>
	{
		let contents = "[foo]\nfaz=far\nfiz=foz\n\n[boo]\nbaz=bar\n";

		let fixture = createFixture( "delete-section", contents );
		innie.delete( fixture, "foo" );

		let actual = readFile( fixture );
		assert.equal( actual, "[boo]\nbaz=bar\n" );
	});
});
