# innie

Read, write, and delete [INI](https://en.wikipedia.org/wiki/INI_file) file entries from the command line.

## Setup

### Requirements

- [Node and NPM](https://nodejs.org)

### Installation

To install **innie** globally:

```
npm install -g @aaashur/innie
```

To run without installing:

```
npx @aaashur/innie <command> <args>
```

## Usage

Given **colors.ini**:

```ini
black=rgb(0, 0, 0)

[red]
100=rgb(255, 245, 245)
200=rgb(254, 215, 215)
300=rgb(254, 178, 178)

[orange]
100=rgb(255, 250, 240)
200=rgb(254, 235, 200)
300=rgb(254, 235, 200)
```

### read

Read the value of top-level key `black`:

```
$ innie read colors.ini black
rgb(0, 0, 0)
```

Read the value of key `100` in section `red`:

```
$ innie read colors.ini red.100
rgb(255, 245, 245)
```

Read the value of all keys in section `orange`:

```
$ innie read colors.ini orange
100=rgb(255, 250, 240)
200=rgb(254, 235, 200)
300=rgb(254, 235, 200)
```

### write

Write the value of new top-level key `white`:

```
$ innie write colors.ini white "rgb(255, 255, 255)"
```

Overwrite the value of existing nested key `300` in section `orange`:

```
$ innie write colors.ini orange.300 "rgb(251, 211, 141)"
```

Create a new section `yellow` and write the value of nested key `100`:

```
$ innie write colors.ini yellow.100 "rgb(255, 255, 240)"
```

### delete

Delete top-level key `white`:

```
$ innie delete colors.ini white
```

Delete nested key `300` in section `orange`:

```
$ innie delete colors.ini orange.300
```

Delete section `yellow` and all of its nested keys:

```
$ innie delete colors.ini yellow
```
