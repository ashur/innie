const dotProp = require( "dot-prop" );
const {readFile, writeFile} = require( "./utils" );
const ini = require( "ini" );

/**
 * Read value from INI file.
 *
 * @param {string} filename
 * @param {string} key
 * @returns {string}
 */
module.exports.read = (filename, key) =>
{
	let contents = readFile( filename );

	let object = ini.decode( contents );
	let value = dotProp.get( object, key );

	if( value === undefined )
	{
		throw new Error( `No such key '${key}'` )
	}

	if( typeof value === "object" )
	{
		return ini.encode( value ).trim();
	}

	return value;
}

/**
 * Write value for key in INI file.
 *
 * @param {string} filename
 * @param {string} key
 * @param {string} value
 * @param {string}
 */
module.exports.write = (filename, key, value) =>
{
	let contents = readFile( filename );

	let object = ini.decode( contents );
	let updatedObject = dotProp.set( object, key, value );

	return writeFile( filename, ini.encode( updatedObject ) );
};

/**
 * Delete key or section in INI file.
 *
 * @param {string} filename
 * @param {string} key
 */
module.exports.delete = (filename, key) =>
{
	let contents = readFile( filename );

	let object = ini.decode( contents );
	if( dotProp.get( object, key ) !== undefined )
	{
		dotProp.delete( object, key );
		writeFile( filename, ini.encode( object ) );
	}
};
