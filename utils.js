const fs = require( "fs" );

/**
 * @param {string} filename
 * @returns {boolean}
 */
function fileExists( filename )
{
	try
	{
		fs.statSync( filename );
	}
	catch( error )
	{
		if( error.code === "ENOENT" )
		{
			return false;
		}
		else
		{
			throw error;
		}
	}

	return true;
}

/**
 * Read contents of file
 *
 * @param {string} filename
 * @return {string}
 * @throws If file does not exist
 */
module.exports.readFile = (filename) =>
{
	if( !fileExists( filename ) )
	{
		throw new Error( `No such file '${filename}'` );
	}

	return fs.readFileSync( filename ).toString();
}

/**
 * Write contents to file
 *
 * @param {string} filename
 * @param {string} contents
 * @return {string}
 * @throws If file does not exist
 */
module.exports.writeFile = (filename, contents) =>
{
	if( !fileExists( filename ) )
	{
		throw new Error( `No such file '${filename}'` );
	}

	return fs.writeFileSync( filename, contents );
}
